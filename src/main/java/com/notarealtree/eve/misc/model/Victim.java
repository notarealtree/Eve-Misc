package com.notarealtree.eve.misc.model;

/**
 * Created by Francis on 11/15/2015.
 */
public class Victim {
    private long shipTypeID;
    private long characterID;
    private String characterName;
    private long corporationID;
    private String corporationName;
    private long allianceID;
    private String allianceName;
    private long factionID;
    private String factionName;
    private long damageTaken;

    public Victim(){}

    public Victim(long shipTypeID, long characterID, String characterName, long corporationID, String corporationName, long allianceID, String allianceName, long factionID, String factionName, long damageTaken) {
        this.shipTypeID = shipTypeID;
        this.characterID = characterID;
        this.characterName = characterName;
        this.corporationID = corporationID;
        this.corporationName = corporationName;
        this.allianceID = allianceID;
        this.allianceName = allianceName;
        this.factionID = factionID;
        this.factionName = factionName;
        this.damageTaken = damageTaken;
    }

    public long getShipTypeID() {
        return shipTypeID;
    }

    public long getCharacterID() {
        return characterID;
    }

    public String getCharacterName() {
        return characterName;
    }

    public long getCorporationID() {
        return corporationID;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public long getAllianceID() {
        return allianceID;
    }

    public String getAllianceName() {
        return allianceName;
    }

    public long getFactionID() {
        return factionID;
    }

    public String getFactionName() {
        return factionName;
    }

    public long getDamageTaken() {
        return damageTaken;
    }
}
