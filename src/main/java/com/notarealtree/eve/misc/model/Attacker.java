package com.notarealtree.eve.misc.model;

/**
 * Created by Francis on 11/15/2015.
 */
public class Attacker {
    private long characterID;
    private String characterName;
    private long corporationID;
    private String corporationName;
    private long allianceID;
    private String allianceName;
    private long factionID;
    private String factionName;
    private double securityStatus;
    private int damageDone;
    private int finalBlow;
    private long weaponTypeID;
    private long shipTypeID;
    
    public Attacker(){}

    public Attacker(long characterID, String characterName, long corporationID, String corporationName, long allianceID, String allianceName, long factionID, String factionName, double securityStatus, int damageDone, int finalBlow, long weaponTypeID, long shipTypeID) {
        this.characterID = characterID;
        this.characterName = characterName;
        this.corporationID = corporationID;
        this.corporationName = corporationName;
        this.allianceID = allianceID;
        this.allianceName = allianceName;
        this.factionID = factionID;
        this.factionName = factionName;
        this.securityStatus = securityStatus;
        this.damageDone = damageDone;
        this.finalBlow = finalBlow;
        this.weaponTypeID = weaponTypeID;
        this.shipTypeID = shipTypeID;
    }

    public long getCharacterID() {
        return characterID;
    }

    public String getCharacterName() {
        return characterName;
    }

    public long getCorporationID() {
        return corporationID;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public long getAllianceID() {
        return allianceID;
    }

    public String getAllianceName() {
        return allianceName;
    }

    public long getFactionID() {
        return factionID;
    }

    public String getFactionName() {
        return factionName;
    }

    public double getSecurityStatus() {
        return securityStatus;
    }

    public int getDamageDone() {
        return damageDone;
    }

    public int getFinalBlow() {
        return finalBlow;
    }

    public long getWeaponTypeID() {
        return weaponTypeID;
    }

    public long getShipTypeID() {
        return shipTypeID;
    }
}
