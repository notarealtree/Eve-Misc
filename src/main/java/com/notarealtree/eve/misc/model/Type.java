package com.notarealtree.eve.misc.model;

/**
 * Created by Francis on 11/16/2015.
 */
public class Type {
    private final long typeID;
    private final long groupID;
    private final String typeName;

    public Type(long typeID, long groupID, String typeName){
        this.typeID = typeID;
        this.groupID = groupID;
        this.typeName = typeName;
    }

    public long getTypeID() {
        return typeID;
    }

    public long getGroupID() {
        return groupID;
    }

    public String getTypeName() {
        return typeName;
    }

}

