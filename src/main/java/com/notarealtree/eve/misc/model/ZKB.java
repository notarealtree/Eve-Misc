package com.notarealtree.eve.misc.model;

/**
 * Created by Francis on 11/15/2015.
 */
public class ZKB {
    private long locationID;
    private String hash;
    private double totalValue;
    private int points;

    public ZKB(){}

    public ZKB(long locationID, String hash, double totalValue, int points) {
        this.locationID = locationID;
        this.hash = hash;
        this.totalValue = totalValue;
        this.points = points;
    }

    public long getLocationID() {
        return locationID;
    }

    public String getHash() {
        return hash;
    }

    public double getTotalValue() {
        return totalValue;
    }

    public int getPoints() {
        return points;
    }
}
