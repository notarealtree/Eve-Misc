package com.notarealtree.eve.misc.model;

/**
 * Created by Francis on 11/15/2015.
 */
public class Item {
    private long typeID;
    private int flag;
    private long qtyDropped;
    private long qtyDestroyed;
    private int singleton;

    public Item(){}

    public Item(long typeID, int flag, long qtyDropped, long qtyDestroyed, int singleton) {
        this.typeID = typeID;
        this.flag = flag;
        this.qtyDropped = qtyDropped;
        this.qtyDestroyed = qtyDestroyed;
        this.singleton = singleton;
    }

    public long getTypeID() {
        return typeID;
    }

    public int getFlag() {
        return flag;
    }

    public long getQtyDropped() {
        return qtyDropped;
    }

    public long getQtyDestroyed() {
        return qtyDestroyed;
    }

    public int getSingleton() {
        return singleton;
    }
}
