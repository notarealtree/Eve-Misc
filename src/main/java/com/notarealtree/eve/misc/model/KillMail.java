package com.notarealtree.eve.misc.model;

import java.util.List;

/**
 * Created by Francis on 11/15/2015.
 */
public class KillMail {
    private long killID;
    private long solarSystemID;
    private String killTime; //TODO: Change to something more sensible
    private long moonID;
    private Victim victim;
    private List<Attacker> attackers;
    private List<Item> items;
    private Position position;
    private ZKB zkb;

    public KillMail(){}

    public KillMail(long killID, long solarSystemID, String killTime, long moonID, Victim victim, List<Attacker> attackers, List<Item> items, Position position, ZKB zkb) {
        this.killID = killID;
        this.solarSystemID = solarSystemID;
        this.killTime = killTime;
        this.moonID = moonID;
        this.victim = victim;
        this.attackers = attackers;
        this.items = items;
        this.position = position;
        this.zkb = zkb;
    }

    public long getKillID() {
        return killID;
    }

    public long getSolarSystemID() {
        return solarSystemID;
    }

    public String getKillTime() {
        return killTime;
    }

    public long getMoonID() {
        return moonID;
    }

    public Victim getVictim() {
        return victim;
    }

    public List<Attacker> getAttackers() {
        return attackers;
    }

    public List<Item> getItems() {
        return items;
    }

    public Position getPosition() {
        return position;
    }

    public ZKB getZkb() {
        return zkb;
    }
}
