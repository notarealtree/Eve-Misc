package com.notarealtree.eve.misc.model;

import java.util.List;

/**
 * Created by Francis on 11/16/2015.
 */
public class TypeResponse {
    private final List<Type> types;
    public TypeResponse(List<Type> typeIDs){
        this.types = typeIDs;
    }

    public List<Type> getTypes() {
        return types;
    }
}
