package com.notarealtree.eve.misc.model;


/**
 * Created by Francis on 11/17/2015.
 */
public class SolarSystem {
    private final long systemID;
    private final long constellationID;
    private final long regionID;
    private final String systemName;
    private final String securityStatus;

    public SolarSystem( long regionID, long constellationID, long systemID, String systemName, String securityStatus) {
        this.systemID = systemID;
        this.constellationID = constellationID;
        this.regionID = regionID;
        this.systemName = systemName;
        this.securityStatus = securityStatus;
    }

    public long getSystemID() {
        return systemID;
    }

    public long getConstellationID() {
        return constellationID;
    }

    public long getRegionID() {
        return regionID;
    }

    public String getSystemName() {
        return systemName;
    }

    public String getSecurityStatus() {
        return securityStatus;
    }
}
