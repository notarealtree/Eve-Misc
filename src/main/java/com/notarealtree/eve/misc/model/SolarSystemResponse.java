package com.notarealtree.eve.misc.model;

import java.util.List;

/**
 * Created by Francis on 11/17/2015.
 */
public class SolarSystemResponse {
    private final List<SolarSystem> systems;

    public SolarSystemResponse(List<SolarSystem> solarSystemList) {
        this.systems = solarSystemList;
    }

    public List<SolarSystem> getSystems() {
        return systems;
    }
}
