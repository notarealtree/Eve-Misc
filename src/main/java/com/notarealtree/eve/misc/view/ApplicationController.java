package com.notarealtree.eve.misc.view;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notarealtree.eve.misc.controller.*;
import com.notarealtree.eve.misc.model.SolarSystemResponse;
import com.notarealtree.eve.misc.model.TypeResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Francis on 11/15/2015.
 */

@RestController
public class ApplicationController {
    private static final String ZKB_BEAN_ID = "zkbConnector";
    private static final String TYPE_CONVERTER_BEAN_ID = "typeIDConverter";
    private static final String SYSTEM_CONVERTER_BEAN_ID = "solarSystemIDConverter";
    private static final String IDS_URL_PARAM = "ids";
    private static final String NAMES_URL_PARAM = "names";
    private static final Logger logger = Logger.getLogger(ApplicationController.class.getName());
    private final ObjectMapper objectMapper = new ObjectMapper();
    private static ApplicationContext applicationContext = new ClassPathXmlApplicationContext(new String[] {"beans.xml"});
    private static TypeIDConverter typeIDConverter = (TypeIDConverter)applicationContext.getBean(TYPE_CONVERTER_BEAN_ID);
    private static SolarSystemIDConverter solarSystemIDConverter = (SolarSystemIDConverter)applicationContext.getBean(SYSTEM_CONVERTER_BEAN_ID);

    private RequestProcessor requestProcessor = new RequestProcessor(typeIDConverter, solarSystemIDConverter);

    private Jedis jedis;

    @Value("${update-interval}")
    private int updateInterval;

    @PostConstruct
    public void postConstruct(){
        JedisPool pool = new JedisPool(new JedisPoolConfig(), "localhost", 6379, 15000);
        jedis = pool.getResource();
        ZKBConnector zkbConnector = (ZKBConnector)applicationContext.getBean(ZKB_BEAN_ID);
        StatUpdater statUpdater = new StatUpdater(new RedisConnector(pool.getResource(), typeIDConverter, solarSystemIDConverter), updateInterval, zkbConnector);
        Thread thread = new Thread(statUpdater);
        thread.start();
    }

    @RequestMapping(value="/")
    public ModelAndView status(){
        return new ModelAndView("redirect:https://github.com/NotARealTree/Eve-Misc");
    }

    @RequestMapping("/type/nametoid")
    public TypeResponse typeNameToID(@RequestParam(NAMES_URL_PARAM) String names){
        TypeResponse cachedResponse = deserialize(names, TypeResponse.class);
        TypeResponse response = cachedResponse != null ? cachedResponse : requestProcessor.typeNameToTypeID(names);
        serializeAndInsert(names, response);
        return response;
    }

    @RequestMapping("/type/idtoname")
    public TypeResponse typeIDToName(@RequestParam(IDS_URL_PARAM) String ids){
        TypeResponse cachedResponse = deserialize(ids, TypeResponse.class);
        TypeResponse response = cachedResponse != null ? cachedResponse : requestProcessor.typeIDToTypeName(ids);
        serializeAndInsert(ids, response);
        return response;
    }

    @RequestMapping("/system/nametoid")
    public SolarSystemResponse systemNameToID(@RequestParam(NAMES_URL_PARAM) String names){
        SolarSystemResponse cachedResponse = deserialize(names, SolarSystemResponse.class);
        SolarSystemResponse response = cachedResponse != null ? cachedResponse : requestProcessor.systemNameToID(names);
        serializeAndInsert(names, response);
        return response;
    }

    @RequestMapping("/system/idtoname")
    public SolarSystemResponse systemIDToName(@RequestParam(IDS_URL_PARAM) String ids){
        SolarSystemResponse cachedResponse = deserialize(ids, SolarSystemResponse.class);
        SolarSystemResponse response = cachedResponse != null ? cachedResponse : requestProcessor.systemIDToName(ids);
        serializeAndInsert(ids, response);
        return response;
    }

    private <T> void serializeAndInsert(String key, T valueToSerialize){
        key = sort(key);
        if(!jedis.exists(key)){
            try {
                jedis.set(key, objectMapper.writeValueAsString(valueToSerialize));
            } catch (JsonProcessingException e) {
                logger.log(Level.WARNING, "Serialization of " + valueToSerialize.getClass().getName() + " failed.");
            }
        }
    }

    private <T> T deserialize(String key, Class<T> type){
        key = sort(key);
        if(!jedis.exists(key)){
            return null;
        }
        try {
            return objectMapper.readValue(jedis.get(key), type);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Deserialization of " + jedis.get(key) + " failed with class " + type.getName());
            return null;
        }
    }

    private String sort(String input){
        return Arrays.asList(input.split(";")).stream().sorted().reduce((first, next) -> first + ";" + next).get();
    }
}
