package com.notarealtree.eve.misc.controller;

import com.notarealtree.eve.misc.model.Type;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Francis on 11/16/2015.
 */
public class TypeIDConverter {
    private Map<String, Type> nameToType;
    private Map<Long, Type> idToType;
    public String typeIDs;

    public TypeIDConverter(String typeIDs){
        this.typeIDs = typeIDs;
    }

    public void parse(){
        nameToType = new HashMap<>();
        idToType = new HashMap<>();
        List<String> lines = Arrays.asList(typeIDs.replace("\r", "").split("\n"));
        for(String line : lines){
            String[] splitLine = line.split(",");
            String lineEnd;
            if(splitLine.length == 3){
                lineEnd = splitLine[2];
            }else if(splitLine.length > 3){
                lineEnd = Arrays.asList(Arrays.copyOfRange(splitLine, 3, splitLine.length)).stream().reduce((i1, i2) -> i1+i2).get();
            }else{
                continue;
            }
            Type tempType = new Type(Long.parseLong(splitLine[0]), Long.parseLong(splitLine[1]), lineEnd);
            nameToType.put(splitLine[2], tempType);
            idToType.put(Long.parseLong(splitLine[0]), tempType);
        }
    }

    public Type convertNameToType(String name){
        return nameToType.get(name);
    }

    public Type convertIDToType(long typeID){
        return idToType.get(typeID);
    }
}
