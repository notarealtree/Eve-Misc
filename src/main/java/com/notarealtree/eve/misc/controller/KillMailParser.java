package com.notarealtree.eve.misc.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notarealtree.eve.misc.model.KillMail;

import java.io.IOException;
import java.util.List;

/**
 * Created by Francis on 11/15/2015.
 */
public class KillMailParser {
    private final ObjectMapper objectMapper;
    public KillMailParser(){
        this.objectMapper = new ObjectMapper();
    }

    public List<KillMail> parseKillMails(String inputJSON) throws IOException {
        List<KillMail> kills = objectMapper.readValue(inputJSON, new TypeReference<List<KillMail>>(){});
        return kills;
    }
}
