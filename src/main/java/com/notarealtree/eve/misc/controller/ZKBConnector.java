package com.notarealtree.eve.misc.controller;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Francis on 11/15/2015.
 */
@Configuration
public class ZKBConnector {
    private final Logger logger = Logger.getLogger(getClass().getName());
    private String userAgent;
    private String url;
    private String modifiers;

    public ZKBConnector(){}

    public ZKBConnector(String url, String modifiers, String userAgent){
        this.url = url;
        this.modifiers = modifiers;
        this.userAgent = userAgent;
    }

    public String getKills() throws UnirestException {
        logger.log(Level.INFO, "Making http request to " + url + modifiers);
        HttpResponse<JsonNode> response = Unirest
                .get(url + modifiers)
                .header("accept-encoding", "gzip")
                .header("user-agent", userAgent)
                .asJson();
        return response.getBody().toString();
    }
}
