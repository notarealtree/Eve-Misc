package com.notarealtree.eve.misc.controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import com.notarealtree.eve.misc.model.KillMail;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Francis on 11/17/2015.
 */
public class StatUpdater implements Runnable{

    private RedisConnector redisConnector;
    private int updateInterval;
    private KillMailParser killMailParser;
    private ZKBConnector zkbConnector;

    public StatUpdater(RedisConnector redisConnector, int updateInterval, ZKBConnector zkbConnector){
        this.redisConnector = redisConnector;
        this.updateInterval = updateInterval;
        this.killMailParser = new KillMailParser();
        this.zkbConnector = zkbConnector;
    }

    @Override
    public void run() {
        while(!Thread.interrupted()){
            try {
                // Filter out killmails we've already processed
                List<KillMail> killMails = killMailParser.parseKillMails(zkbConnector.getKills());
                killMails.stream()
                        .filter(mail -> redisConnector.containsID(mail.getKillID()))
                        .collect(Collectors.toList());

                // Insert Killmails into redis
                killMails.forEach(mail -> redisConnector.insertKillMail(mail));

                // Insert Killmail IDS into redis
                killMails.forEach(mail -> redisConnector.insertID(mail));

                // Insert Items and counts into redis
                killMails.forEach(mail -> redisConnector.insertKillMailItems(mail));

            } catch (IOException e) {
                e.printStackTrace();
            } catch (UnirestException e) {
                e.printStackTrace();
            }

            try {
                Thread.sleep(updateInterval);
            } catch (InterruptedException e) {
                e.printStackTrace(); // TODO: Decide on handling
            }
        }
    }


}
