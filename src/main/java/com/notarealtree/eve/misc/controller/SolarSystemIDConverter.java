package com.notarealtree.eve.misc.controller;

import com.notarealtree.eve.misc.model.SolarSystem;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Francis on 11/16/2015.
 */
public class SolarSystemIDConverter {
    private final String solarSystemIDs;
    private Map<String, SolarSystem> nameToSolarSystem;
    private Map<Long, SolarSystem> idToSolarSystem;

    public SolarSystemIDConverter(String solarSystemIDs){
        this.solarSystemIDs = solarSystemIDs;
    }

    public void parse(){
        nameToSolarSystem = new HashMap<>();
        idToSolarSystem = new HashMap<>();
        List<String> lines = Arrays.asList(solarSystemIDs.replace("\r", "").split("\n"));
        for(String line : lines){
            String[] splitLine = line.split(",");
            String securityStatus = parseSecurity(splitLine[4]);
            SolarSystem tempSystem = new SolarSystem(Long.parseLong(splitLine[0]), Long.parseLong(splitLine[1]), Long.parseLong(splitLine[2]), splitLine[3], securityStatus);
            nameToSolarSystem.put(splitLine[3], tempSystem);
            idToSolarSystem.put(Long.parseLong(splitLine[2]), tempSystem);
        }
    }

    private String parseSecurity(String securityStatus){
        double security = Double.parseDouble(securityStatus);
        if(security >= 0.45){
            return "high";
        }else if(security >= 0){
            return "low";
        }else if(security == -0.99){
            return "wormhole";
        }else{
            return "null";
        }
    }

    public SolarSystem convertNameToSystem(String name){
        return nameToSolarSystem.get(name);
    }

    public SolarSystem convertIdToSystem(long id){
        return idToSolarSystem.get(id);
    }
}
