package com.notarealtree.eve.misc.controller;

import com.notarealtree.eve.misc.model.SolarSystem;
import com.notarealtree.eve.misc.model.SolarSystemResponse;
import com.notarealtree.eve.misc.model.Type;
import com.notarealtree.eve.misc.model.TypeResponse;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Francis on 11/16/2015.
 */
public class RequestProcessor {
    private TypeIDConverter typeIDConverter;
    private SolarSystemIDConverter solarSystemIDConverter;

    public RequestProcessor(TypeIDConverter typeIDConverter, SolarSystemIDConverter solarSystemIDConverter){
        this.typeIDConverter = typeIDConverter;
        this.solarSystemIDConverter = solarSystemIDConverter;
    }

    public TypeResponse typeNameToTypeID(String names){
        List<String> nameList = Arrays.asList(names.split(";"));
        List<Type> typeList = nameList
                .parallelStream()
                .map(typeIDConverter::convertNameToType)
                .collect(Collectors.toList());
        return new TypeResponse(typeList);
    }

    public TypeResponse typeIDToTypeName(String ids){
        List<Long> idList = Arrays.asList(ids.split(";"))
                .parallelStream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
        List<Type> typeList = idList
                .parallelStream()
                .map(typeIDConverter::convertIDToType)
                .collect(Collectors.toList());
        return new TypeResponse(typeList);
    }

    public SolarSystemResponse systemNameToID(String names){
        List<String> nameList = Arrays.asList(names.split(";"));
        List<SolarSystem> solarSystemList = nameList
                .parallelStream()
                .map(solarSystemIDConverter::convertNameToSystem)
                .collect(Collectors.toList());
        return new SolarSystemResponse(solarSystemList);
    }

    public SolarSystemResponse systemIDToName(String ids){
        List<Long> idList = Arrays.asList(ids.split(";"))
                .parallelStream()
                .map(Long::parseLong)
                .collect(Collectors.toList());
        List<SolarSystem> solarSystemList = idList
                .parallelStream()
                .map(solarSystemIDConverter::convertIdToSystem)
                .collect(Collectors.toList());
        return new SolarSystemResponse(solarSystemList);
    }
}
