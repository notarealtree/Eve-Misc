package com.notarealtree.eve.misc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.notarealtree.eve.misc.model.Item;
import com.notarealtree.eve.misc.model.KillMail;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import redis.clients.jedis.Jedis;

import java.util.List;

/**
 * Created by Francis on 11/17/2015.
 */
public class RedisConnector {
    private TypeIDConverter typeIDConverter;
    private SolarSystemIDConverter solarSystemIDConverter;
    private Jedis jedis;
    private final String IDSET = "ids";
    private final String KILLSET = "killmail";
    private final String ITEMSET = "items";
    private final ObjectMapper mapper = new ObjectMapper();
    private final DateTimeFormatter zkbDateTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
    private final DateTimeFormatter redisFormat = DateTimeFormat.forPattern("yyyyMMddHH");

    public RedisConnector(Jedis jedis, TypeIDConverter typeIDConverter, SolarSystemIDConverter solarSystemIDConverter){
        this.jedis = jedis;
        this.typeIDConverter = typeIDConverter;
        this.solarSystemIDConverter = solarSystemIDConverter;
    }

    public boolean containsID(long killID){
        return jedis.sismember(IDSET, String.valueOf(killID));
    }

    public void insertKillMail(KillMail killMail){
        DateTime killTime = DateTime.parse(killMail.getKillTime(), zkbDateTimeFormat);
        double redisKey = Double.parseDouble(redisFormat.print(killTime));
        String killMailAsJSON;
        try {
            killMailAsJSON = mapper.writeValueAsString(killMail);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return;
        }
        jedis.zadd(KILLSET, redisKey, killMailAsJSON);
    }

    public void insertID(KillMail killMail){
        jedis.sadd(IDSET, String.valueOf(killMail.getKillID()));
    }

    public void insertKillMailItems(KillMail killMail){
        String securityClass = solarSystemIDConverter.convertIdToSystem(killMail.getSolarSystemID()).getSecurityStatus();
        List<Item> items = killMail.getItems();
        // Make the ship an item
        items.add(new Item(killMail.getVictim().getShipTypeID(), 0, 0, 1, 0));

        for(Item item : items){
            String itemName = typeIDConverter.convertIDToType(item.getTypeID()).getTypeName();
            double sumOfItem = item.getQtyDestroyed() + item.getQtyDropped();

            // Insert into general item set
            jedis.zincrby(ITEMSET, sumOfItem, itemName);

            // Insert into specific to sec status set
            jedis.zincrby(ITEMSET + ":" + securityClass, sumOfItem, itemName);
        }
    }
}
