import com.notarealtree.eve.misc.controller.KillMailParser;
import com.notarealtree.eve.misc.model.KillMail;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by Francis on 11/15/2015.
 */
public class KillMailParserTest {
    private static String singleMailAsJSON;
    private static String fourMailsAsJSON;

    @BeforeClass
    public static void setUp() throws IOException {
        singleMailAsJSON = Files.readAllLines(Paths.get("src/test/resources/singlemail.json")).stream().reduce((l1, l2) -> l1 + l2).get();
        fourMailsAsJSON = Files.readAllLines(Paths.get("src/test/resources/fourmails.json")).stream().reduce((l1, l2) -> l1 + l2).get();
    }

    @Test
    public void testParseSingleItem() throws IOException {
        // Given
        KillMailParser killMailParser = new KillMailParser();
        long expectedKillID = 50179987;
        int expectedListLength = 1;
        int expectedNumberOfAttackers = 7;
        int expectedNumberOfItems = 37;
        long expectedVictimID = 901320273;
        double expectedPositionX = 276157204069.35;
        String expectedZKBHash = "500c79aa050044804a86e6b92e67d697fdd0165a";

        // When
        List<KillMail> kills = killMailParser.parseKillMails(singleMailAsJSON);

        // Then
        assertEquals("Got the correct list length", expectedListLength, kills.size());
        assertEquals("KillID is correct", expectedKillID, kills.get(0).getKillID());
        assertEquals("Number of Attackers is correct", expectedNumberOfAttackers, kills.get(0).getAttackers().size());
        assertEquals("Number of Items is correct", expectedNumberOfItems, kills.get(0).getItems().size());
        assertEquals("Victim ID is correct", expectedVictimID, kills.get(0).getVictim().getCharacterID());
        assertEquals("Position X is correct", expectedPositionX, kills.get(0).getPosition().getX(), 0.0);
        assertEquals("Checking ZKB Hash ID is correct", expectedZKBHash, kills.get(0).getZkb().getHash());
    }

    @Test
    public void testParseFourItems() throws IOException {
        // Given
        KillMailParser killMailParser = new KillMailParser();
        int expectedNumberOfKills = 4;

        // When
        List<KillMail> kills = killMailParser.parseKillMails(fourMailsAsJSON);

        // Then
        assertEquals("Got the correct list length", expectedNumberOfKills, kills.size());
    }
}
