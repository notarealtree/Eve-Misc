import com.mashape.unirest.http.exceptions.UnirestException;
import com.notarealtree.eve.misc.controller.KillMailParser;
import com.notarealtree.eve.misc.controller.ZKBConnector;
import com.notarealtree.eve.misc.model.KillMail;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;


/**
 * Created by Francis on 11/15/2015.
 */
public class ZKBConnectorTest {

    @Test
    public void TestZKBConnector() throws UnirestException, IOException {
        // Given
        /*
        ZKBConnector zkbConnector = new ZKBConnector("https://zkillboard.com/api", "/killID/50179987/", "fscreene@icloud.com, testing json response, sorry =/");
        String json = Files.readAllLines(Paths.get("src/test/resources/singlemail.json")).stream().map(String::trim).reduce((l1, l2) -> l1 + l2).get();
        KillMailParser killMailParser = new KillMailParser();
        KillMail expectedKillMail = killMailParser.parseKillMails(json).get(0);

        // When
        String actualJSON = zkbConnector.getKills();
        KillMail actualKillMail = killMailParser.parseKillMails(actualJSON).get(0);

        // Then
        assertReflectionEquals("Checking actual json equals our expected json", expectedKillMail, actualKillMail);
    */
    }
}
