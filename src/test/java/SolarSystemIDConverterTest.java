import com.notarealtree.eve.misc.controller.SolarSystemIDConverter;
import com.notarealtree.eve.misc.model.SolarSystem;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

/**
 * Created by Francis on 11/17/2015.
 */
public class SolarSystemIDConverterTest {
    private static SolarSystemIDConverter solarSystemIDConverter;
    @BeforeClass
    public static void setup() throws IOException {
        String input = Files.readAllLines(Paths.get("src/main/resources/solarSystemIDs.csv")).stream().map(i -> i + "\n").reduce((i1, i2) -> i1 + i2).get();
        solarSystemIDConverter = new SolarSystemIDConverter(input);
        solarSystemIDConverter.parse();
    }

    @Test
    public void testIDToName(){
        // Given
        String expectedTypeName = "Amarr";
        String expectedSecurityStatus = "high";

        // When
        SolarSystem solarSystem = solarSystemIDConverter.convertIdToSystem(30002187);

        // Then
        assertEquals("Resulting typename equals expected result", expectedTypeName, solarSystem.getSystemName());
        assertEquals("Checking we got the correct security status", expectedSecurityStatus, solarSystem.getSecurityStatus());
    }

    @Test
    public void testNameToID(){
        // Given
        long expectedTypeID = 30002187;

        // When
        long typeID = solarSystemIDConverter.convertNameToSystem("Amarr").getSystemID();

        // Then
        assertEquals("Resulting typeID equals expected result", expectedTypeID, typeID);
    }

    @Test
    public void testSecurityStatusParser(){
        // Given
        String[] sec = {"high", "low", "null", "wormhole"};
        List<String> expectedSecurityStatus = Arrays.asList(sec);
        String[] names = {"Amarr", "Tama", "FD53-H", "J105934"};
        List<String> systemNames = Arrays.asList(names);

        // When
        List<String> actualSecurityStatus = systemNames
                .stream()
                .map(i -> solarSystemIDConverter.convertNameToSystem(i).getSecurityStatus())
                .collect(Collectors.toList());

        // Then
        assertReflectionEquals("Checking we got the correct results for our parsing", expectedSecurityStatus, actualSecurityStatus);
    }

}
