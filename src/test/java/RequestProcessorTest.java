import com.notarealtree.eve.misc.controller.RequestProcessor;
import com.notarealtree.eve.misc.controller.SolarSystemIDConverter;
import com.notarealtree.eve.misc.controller.TypeIDConverter;
import com.notarealtree.eve.misc.model.SolarSystem;
import com.notarealtree.eve.misc.model.Type;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

/**
 * Created by Francis on 11/17/2015.
 */
public class RequestProcessorTest {
    private static RequestProcessor requestProcessor;

    @BeforeClass
    public static void setUp() throws IOException {
        SolarSystemIDConverter solarSystemIDConverter = new SolarSystemIDConverter(Files.readAllLines(Paths.get("src/main/resources/solarSystemIDs.csv")).stream().map(i -> i + "\n").reduce((i1, i2) -> i1 + i2).get());
        TypeIDConverter typeIDConverter = new TypeIDConverter(Files.readAllLines(Paths.get("src/main/resources/typeIDs.csv")).stream().map(i -> i + "\n").reduce((i1, i2) -> i1 + i2).get());
        solarSystemIDConverter.parse();
        typeIDConverter.parse();
        requestProcessor = new RequestProcessor(typeIDConverter, solarSystemIDConverter);
    }

    @Test
    public void testSingleTypeIDToName(){
        // Given
        String typeID = "193";
        String expectedName = "EMP M";

        // When
        String actualName = requestProcessor.typeIDToTypeName(typeID).getTypes().get(0).getTypeName();

        // Then
        assertEquals("Result matches expected type name", expectedName, actualName);
    }

    @Test
    public void testMultipleTypeIDToName(){
        // Given
        String typeIDs = "193;194";
        String firstExpectedName = "EMP M";
        String secondExpectedName = "Carbonized Lead L";

        // When
        List<Type> responses = requestProcessor.typeIDToTypeName(typeIDs).getTypes();
        String actualFirstName = responses.get(0).getTypeName();
        String actualSecondName = responses.get(1).getTypeName();

        // Then
        assertEquals("Result matches expected type name", firstExpectedName, actualFirstName);
        assertEquals("Result matches expected type name", secondExpectedName, actualSecondName);
    }

    @Test
    public void testSingleTypeNameToID(){
        // Given
        long expectedTypeID = 193;
        String name = "EMP M";

        // When
        long actualTypeID = requestProcessor.typeNameToTypeID(name).getTypes().get(0).getTypeID();

        // Then
        assertEquals("Result matches expected type name", expectedTypeID, actualTypeID);
    }

    @Test
    public void testMultipleTypeNameToID(){
        // Given
        String names = "EMP M;Carbonized Lead L";
        long firstExpectedTypeID = 193;
        long secondExpectedTypeID = 194;

        // When
        List<Type> responses = requestProcessor.typeNameToTypeID(names).getTypes();
        long actualFirstID = responses.get(0).getTypeID();
        long actualSecondID = responses.get(1).getTypeID();

        // Then
        assertEquals("Result matches expected type name", firstExpectedTypeID, actualFirstID);
        assertEquals("Result matches expected type name", secondExpectedTypeID, actualSecondID);
    }


    @Test
    public void testSingleSystemIDToName(){
        // Given
        String typeID = "30002187";
        String expectedName = "Amarr";

        // When
        String actualName = requestProcessor.systemIDToName(typeID).getSystems().get(0).getSystemName();

        // Then
        assertEquals("Result matches expected type name", expectedName, actualName);
    }

    @Test
    public void testMultipleSystemIDToName(){
        // Given
        String systemIDs = "30002187;30000021";
        String firstExpectedName = "Amarr";
        String secondExpectedName = "Kuharah";

        // When
        List<SolarSystem> responses = requestProcessor.systemIDToName(systemIDs).getSystems();
        String actualFirstName = responses.get(0).getSystemName();
        String actualSecondName = responses.get(1).getSystemName();

        // Then
        assertEquals("Result matches expected type name", firstExpectedName, actualFirstName);
        assertEquals("Result matches expected type name", secondExpectedName, actualSecondName);
    }

    @Test
    public void testSingleSystemNameToID(){
        // Given
        long expectedSystemID = 30002187;
        String name = "Amarr";

        // When
        long actualSystemID = requestProcessor.systemNameToID(name).getSystems().get(0).getSystemID();

        // Then
        assertEquals("Result matches expected type name", expectedSystemID, actualSystemID);
    }

    @Test
    public void testMultipleSystemNameToID(){
        // Given
        String names = "Amarr;Kuharah";
        long firstExpectedSystemID = 30002187;
        long secondExpectedSystemID = 30000021;

        // When
        List<SolarSystem> responses = requestProcessor.systemNameToID(names).getSystems();
        long actualFirstID = responses.get(0).getSystemID();
        long actualSecondID = responses.get(1).getSystemID();

        // Then
        assertEquals("Result matches expected type name", firstExpectedSystemID, actualFirstID);
        assertEquals("Result matches expected type name", secondExpectedSystemID, actualSecondID);
    }

}
