import com.mashape.unirest.http.exceptions.UnirestException;
import com.notarealtree.eve.misc.controller.KillMailParser;
import com.notarealtree.eve.misc.controller.RedisConnector;
import com.notarealtree.eve.misc.controller.StatUpdater;
import com.notarealtree.eve.misc.controller.ZKBConnector;
import com.notarealtree.eve.misc.model.KillMail;
import com.notarealtree.eve.misc.model.ZKB;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Francis on 11/18/2015.
 */
public class StatUpdaterTest {
    private static String singleMailAsJSON;

    @Before
    public void setup() throws IOException {
        singleMailAsJSON = Files.readAllLines(Paths.get("src/test/resources/singlemail.json")).stream().reduce((l1, l2) -> l1 + l2).get();
    }

    @Test
    public void testRunWithSingleMail() throws InterruptedException, UnirestException, IOException {
        // Given
        ZKBConnector connector = mock(ZKBConnector.class);
        when(connector.getKills()).thenReturn(singleMailAsJSON);
        RedisConnector redisConnector = mock(RedisConnector.class);
        when(redisConnector.containsID(anyLong())).thenReturn(false);
        StatUpdater statUpdater = new StatUpdater(redisConnector, 3600000, connector);

        // When
        Thread statThread = new Thread(statUpdater);
        statThread.start();
        Thread.sleep(1000);

        // Then
        verify(connector).getKills();
        verify(redisConnector).containsID(50179987);
        verify(redisConnector).insertID(any());
    }
}
