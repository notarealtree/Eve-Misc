import com.notarealtree.eve.misc.controller.TypeIDConverter;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

/**
 * Created by Francis on 11/16/2015.
 */
public class TypeIDConverterTest {
    @Test
    public void testIDToName() throws IOException {
        // Given
        String input = Files.readAllLines(Paths.get("src/main/resources/typeIDs.csv")).stream().map(i -> i + "\n").reduce((i1, i2) -> i1 + i2).get();
        TypeIDConverter typeIDConverter = new TypeIDConverter(input);
        typeIDConverter.parse();
        String expectedTypeName = "EMP M";

        // When
        String typename = typeIDConverter.convertIDToType(193).getTypeName();

        // Then
        assertEquals("Resulting typename equals expected result", expectedTypeName, typename);
    }

    @Test
    public void testNameToID() throws IOException {
        // Given
        String input = Files.readAllLines(Paths.get("src/main/resources/typeIDs.csv")).stream().map(i -> i + "\n").reduce((i1, i2) -> i1 + i2).get();
        TypeIDConverter typeIDConverter = new TypeIDConverter(input);
        typeIDConverter.parse();
        long expectedTypeID = 193;

        // When
        long typeID = typeIDConverter.convertNameToType("EMP M").getTypeID();

        // Then
        assertEquals("Resulting typeID equals expected result", expectedTypeID, typeID);
    }
}
