[![Build Status](https://travis-ci.org/NotARealTree/Eve-Misc.svg?branch=master)](https://travis-ci.org/NotARealTree/Eve-Misc)

# Miscellaneous Eve-Things Api


## Routes

Available routes for various API calls:

---

### ID Conversion

The following routes allow easy (two-way) conversion of type- and solar system ids and names. Names can be passed on their own or as a semicolon-separated list. If possible, group requests for type names and/or ids into a single request to reduce load on the server.

All requests return **JSON** and a sample request and response can be found below the table.

| Route | Method | Parameters | Response |
| ----- | ------ | ---------- | -------- |
| /type/nametoid | GET | names | {"typeID":193,"groupID":83,"typeName":"EMP M"} |
| /type/idtoname | GET | ids | {"typeID":193,"groupID":83,"typeName":"EMP M"} |
| /system/nametoid | GET | names | {"systemID":30002187,"constellationID":20000322,"regionID":10000043,"systemName":"Amarr","securityStatus":"high"} |
| /system/idtoname | GET | ids | {"systemID":30002187,"constellationID":20000322,"regionID":10000043,"systemName":"Amarr","securityStatus":"high"} |

#### Sample Request

**Request**: /system/nametoid?names=Amarr
**Response**: {"systemID":30002187,"constellationID":20000322,"regionID":10000043,"systemName":"Amarr","securityStatus":"high"}

---

### Technology

This API is implemented using the [Spring Rest Framework](https://spring.io/) for Java 1.8. The primary data storage mechanism is based on [redis](https://redis.io) using the [Jedis Java library](jedis-link) which is also used in the caching layer.

Other libraries used in this API are [Unirest](unirest-link) for making http requests to [ZKB](https://zkillboard.com/) and [Unitils](unitils-link) for Unit-Testing.


---

## To-Do

- Make sure that lower case names still get matches
- Add actual address to readme
- Request Logging
- Add redis to travis script
- Unit Test / Integration Test Frontend controller

---

# License

This project is licensed under the MIT license, a copy of which can be found in this repo.
